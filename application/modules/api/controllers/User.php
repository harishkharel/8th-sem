<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/MY_REST_Controller.php';
require  'vendor/autoload.php';

use \Firebase\JWT\JWT;

class User extends MY_REST_Controller {

	public function __construct()
	{		
		parent::__construct();
		$this->load->library('encrypt');
		$this->load->model("User_model", 'user');
	}
	
	public function index_get()
	{
		$token = $this->validate_token($this->input->get_request_header(X_AUTH_TOKEN));
		$id = $this->input->get("id");
		$data  =  $this->user->getAll($id);
		$dataArr = array();
		foreach($data as $ind=>$val){
			$photoName = "";
			if(!empty($val->photo)){
				$photoName = base_url("uploads/userprofile/").$val->photo;
			}
			$val->photo = $photoName; 
			$dataArr[] = $val;
		}
		return $this->set_response(
			$dataArr,
			"Data successful",
			REST_Controller::HTTP_OK
		);
	}
	public function index_post()
	{
		//printr()
		//Validate user
		$token = $this->validate_token($this->input->get_request_header(X_AUTH_TOKEN));
		//printr($token, true);
		$this->load->library('form_validation');

		// Set validations
		$this->form_validation->set_rules('name', 'Full Name', 'required|trim|min_length[1]|max_length[100]');
		$this->form_validation->set_rules('username', 'User Name', 'required|alpha|trim|min_length[1]|max_length[50]');
		$this->form_validation->set_rules('email', 'Email Address', 'required|trim|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[5]|max_length[10]');
		
		// Set data to validate
		$this->form_validation->set_data($this->post());
		
		// Run Validations
	
		if ($this->form_validation->run() == FALSE) {
			return $this->set_response(
				array(),
				validation_errors(),
				REST_Controller::HTTP_BAD_REQUEST
			);
		}
		
		// Check email availability 
		//$email_available = $this->User_model->check_email_availability($this->post('email'));
		/*
		if(!$email_available){
			return $this->set_response(
				array(), 
				$this->lang->line('text_duplicate_email'),
				REST_Controller::HTTP_CONFLICT
			);
		}
		*/
		$config = array();
		$config['upload_path'] = "./uploads/userprofile/";
		$config['allowed_types'] = "jpg|png|jpeg";
		//$config['max_size'] = 2048;
		//$config['max_width'] = 800;
		//$config['max_height'] = 600;
		$this->load->library("upload", $config);
		$uploaded = $this->upload->do_upload('userphoto');
		$uploadedFile = "";
		if($uploaded){
			$uploadData = $this->upload->data();
			$uploadedFile = $uploadData['file_name'];
		}else{
			//printr($this->upload->display_errors());
		}

		$user_data = array(
						"full_name" => $this->input->post("name"),
						"user_name" => $this->input->post("username"),
						"email_address" => $this->input->post("email"),
						"password" => password_hash($this->input->post("password"), PASSWORD_DEFAULT),
						"photo" => $uploadedFile,
						"user_type" => "user",
						"status" => 1,
						"activated" => 1
					);
		//printr($user_data, true);
		// Finally save the user			
		$user_id = $this->user->add($user_data);

		if(!$user_id){
			return $this->set_response(
				array(),
				"Unable to create user",
				REST_Controller::HTTP_INTERNAL_SERVER_ERROR
			);
		}
			
		return $this->set_response(
			array(),
			"User created",
			REST_Controller::HTTP_CREATED
		);
	}

	public function check_email_availability_get()
	{
		$this->load->library('form_validation');

		// Validating inputs
		$this->form_validation->set_rules('email', 'email', 'required|valid_email');
		$this->form_validation->set_data($this->get());
		
		if ($this->form_validation->run() == false) {
			return $this->set_response(
				array(),
				$this->lang->line('text_invalid_params'),
				REST_Controller::HTTP_BAD_REQUEST
			);
		}

		$this->load->model('User_model');

		// Check email availability 
		$email_available = $this->User_model->check_email_availability($this->get('email'));
		
		if(!$email_available){
			return $this->set_response(
				array(),
				$this->lang->line('text_duplicate_email'),
				REST_Controller::HTTP_CONFLICT

			);
		}

		return $this->set_response(
			array(),
			$this->lang->line('text_email_available'),
			REST_Controller::HTTP_OK
		);
	}

	public function update_put($id = null)
	{
		$this->load->library('form_validation');

		// Set validations
		$this->form_validation->set_rules('name', 'name', 'alpha|trim|min_length[1]|max_length[50]');
		$this->form_validation->set_rules('email', 'email', 'valid_email');

		// Set data to validate
		$this->form_validation->set_data($this->post());

		//Run Validations
		if ($this->form_validation->run() == FALSE) {
			return $this->set_response(
				array(),
				$this->lang->line('text_invalid_params'),
				REST_Controller::HTTP_BAD_REQUEST
			);
		}

		// Check email availability
		if(isset($data['email'])){
			$email_available = $this->check_email_availability($data['email'], $data['id']);

			if($email_available['status'] !== true){
				return $email_available;
			}
		}

		// Getting needed user data
		$user_data = $this->form_validation->need_data_as($data, array('name'=>null, 'email'=>null));


		$this->load->model('User_model');
		$data = $this->put();
		$data['id'] = $id;
		$res = $this->User_model->update_profile($data);
		$this->set_response($res, $res['statusCode']);
	}

	public function login_post(){
		//echo password_hash("password", PASSWORD_DEFAULT);
		//die();
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			return $this->set_response(
				array(),
				validation_errors(),
				REST_Controller::HTTP_BAD_REQUEST
			);
		} else {
			$username = $this->input->post("username");
			$password = $this->input->post("password");
			$login = $this->user->check_login($username, $password);
			if($login){
				$this->load->helper('date');
				$timestamp = now();

				$token = array(
					"user_type" => $login->user_type,
					"userdetail"=>array("id"=> $login->id , "username" => $login->user_name, "email" => $login->email_address, "login_time" => $timestamp ),
					"iat" => $timestamp
				);
				$jwt = JWT::encode($token, $this->config->item('jwt_key'));
				$this->set_response(array("token"=>$jwt),"Login SuccessFully.!",MY_REST_Controller::HTTP_OK);
			}else{
				return $this->set_response(
					array(),
					"Username and password not matching",
					REST_Controller::HTTP_UNAUTHORIZED
				);
			}
			//$this->input->post()
		}
	}

}

