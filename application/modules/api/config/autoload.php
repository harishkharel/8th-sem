<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('database', 'form_validation', 'session');

$autoload['helper'] = array('url', 'form', 'rest', "common");

$autoload['language'] = array('api');

$autoload['config'] = array('rest');