<?php
class Restaurants_model extends CI_Model {
	private $validations = array('email'=>'required|valid_email',"password"=>"required|min_length[5]|max_length[20]");
	function __construct() {
		
		parent::__construct();

		//Table Name
		$this->table_name = "restaurants";
	}

	function getAll($id = 0){
		if(!empty($id)){
            $data = $this->db->get_where($this->table_name, ['id' => $id])->row_array();
        }else{
            $data = $this->db->get($this->table_name)->result();
        }
        return $data;
	}
}

?>