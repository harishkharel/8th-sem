<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurants extends MX_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("Restaurants_model", 'restaurant');
		$this->load->model("api/User_model", 'user');
	}
	function index(){
		$data  =  $this->restaurant->getAll();
		print_r($data);
		echo "<h1>Hello Restaurant</h1>";
	}
}